package com.crycl.abdel_samie.favourite_movies

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.crycl.abdel_samie.database.MoviesDatabaseDao

class FavouriteMoviesViewModelFactory(
    private val dataSource: MoviesDatabaseDao,
    private val application: Application) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(FavouriteMoviesViewModel::class.java)) {
            return FavouriteMoviesViewModel(dataSource, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
