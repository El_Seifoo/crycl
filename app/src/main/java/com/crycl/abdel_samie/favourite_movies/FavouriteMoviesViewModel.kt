package com.crycl.abdel_samie.favourite_movies

import android.app.Activity
import android.app.Application
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.core.content.FileProvider
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.crycl.abdel_samie.R
import com.crycl.abdel_samie.database.MoviesDatabaseDao
import com.crycl.abdel_samie.models.Movie
import kotlinx.coroutines.*
import okhttp3.internal.notify
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class FavouriteMoviesViewModel(
    val database: MoviesDatabaseDao,
    application: Application
) : AndroidViewModel(application) {

    // viewModelJob allows us to cancel all coroutines started by this ViewModel.
    private val viewModelJob = Job()

    /*
        Coroutine scope keeps track of all coroutines started by this ViewModel.

        because of passing viewModelJob any coroutine started in this uiScope can be cancelled by calling `viewModelJob.cancel()`

        all coroutines started in uiScope will launch in [Dispatchers.Main] which is the main thread on Android
     */
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    // communicator between viewModel and its view
    lateinit var viewListener: ViewListener

    // flag to know if retrieved movies list from internal storage is empty or not
    private val _emptyListTextView = MutableLiveData<Int>(View.VISIBLE)
    val emptyListTextView: LiveData<Int>
        get() = _emptyListTextView

    private val _moviesList = MutableLiveData<MutableList<Movie>>()
    val moviesList: LiveData<MutableList<Movie>>
        get() = _moviesList


    init {
        initFavourites()
    }

    private fun initFavourites() {
        if (_moviesList.value == null)
        // launch coroutine
            uiScope.launch {
                val movies = fetchFavourites()
                if (movies == null) {
                    _moviesList.value = mutableListOf()
                    _emptyListTextView.value = View.VISIBLE
                } else if (movies.isEmpty()) {
                    _emptyListTextView.value = View.VISIBLE
                } else {
                    _emptyListTextView.value = View.GONE
                    _moviesList.value = movies
                }
            }
    }

    private suspend fun fetchFavourites(): MutableList<Movie> {
        return withContext(Dispatchers.IO) {
            // fetch all movies in the internal storage
            database.getAllMovies()
        }
    }

    fun removeFavourite(movie: Movie, index: Int) {
        uiScope.launch {
            withContext(Dispatchers.IO) {
                database.removeMovie(movie)
            }
            // remove movie from the database table
            withContext(Dispatchers.Main) {
                var list = _moviesList.value
                if (list != null) {
                    list.removeAt(index)
                    if (list.isEmpty()) {
                        _emptyListTextView.value = View.VISIBLE
                    }
                    _moviesList.value = list
                }

            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    fun startSharing(view: ImageView, movieTitle: String) {
        // Get access to the URI for the bitmap
        val bmpUri = getLocalBitmapUri(view, movieTitle)
        if (bmpUri != null) {
            // Construct a ShareIntent with link to image
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri)
            shareIntent.type = "image/*"
            // Launch sharing dialog for image
            viewListener.startSharing(shareIntent)

        } else {
            // user did'n get bitmap uri, sharing failed
            viewListener.showToastMessage(getApplication<Application>().getString(R.string.something_went_wrong))
        }
    }

    private fun getLocalBitmapUri(imageView: ImageView, movieTitle: String): Uri? {
        // Extract Bitmap from ImageView drawable
        val drawable = imageView.drawable
        var bmp: Bitmap? = null
        bmp = if (drawable is BitmapDrawable) {
            (imageView.drawable as BitmapDrawable).bitmap
        } else {
            return null
        }
        // Store image to default external storage directory
        var bmpUri: Uri? = null
        try {
            // This way, you don't need to request external read/write permission.
            val file = File(
                viewListener.getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                "share_image_$movieTitle.png"
            )
            val out = FileOutputStream(file)
            bmp.compress(Bitmap.CompressFormat.PNG, 100, out)
            out.close()
            bmpUri = FileProvider.getUriForFile(
                viewListener.getActivity().baseContext,
                viewListener.getActivity().applicationContext.packageName + ".provider",
                file
            );
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return bmpUri
    }


    interface ViewListener {
        fun showToastMessage(message: String)
        fun startSharing(shareIntent: Intent)
        fun getActivity(): Activity
    }

}

