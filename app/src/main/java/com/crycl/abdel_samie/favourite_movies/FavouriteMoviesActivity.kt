package com.crycl.abdel_samie.favourite_movies

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.crycl.abdel_samie.MoviesAdapter
import com.crycl.abdel_samie.R
import com.crycl.abdel_samie.database.MoviesDatabase
import com.crycl.abdel_samie.databinding.ActivityFavouriteMoviesBinding
import com.crycl.abdel_samie.models.Movie

class FavouriteMoviesActivity : AppCompatActivity(), MoviesAdapter.OnItemClicked,
    FavouriteMoviesViewModel.ViewListener {
    private lateinit var viewModel: FavouriteMoviesViewModel
    private lateinit var binding: ActivityFavouriteMoviesBinding
    private lateinit var moviesList: RecyclerView
    private lateinit var adapter: MoviesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // initialize data binding
        binding = DataBindingUtil.setContentView(this, R.layout.activity_favourite_movies)

        // initialize FavouriteMoviesViewModel
        val viewModelFactory = FavouriteMoviesViewModelFactory(
            MoviesDatabase.getInstance(application).moviesDatabaseDao, application
        )
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(FavouriteMoviesViewModel::class.java)
        viewModel.viewListener = this
        // setting dataBinding variables
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        // initialize recycler view
        moviesList = binding.favouriteList
        moviesList.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        adapter = MoviesAdapter(this)
        moviesList.adapter = adapter



        // observe any changes to moviesList in viewModel to update favourites list
        viewModel.moviesList.observe(this,
            Observer { movies -> adapter.setMovies(movies) })


        val actionBar = supportActionBar
        actionBar!!.setDisplayHomeAsUpEnabled(true)
        actionBar!!.title = getString(R.string.favourites)

    }

    override fun startSharing(shareIntent: Intent) {
        startActivity(Intent.createChooser(shareIntent, getString(R.string.share_poster_via)))
    }

    override fun getActivity() = this

    override fun onClick(movie: Movie, index: Int) {
        viewModel.removeFavourite(movie, index)
    }

    override fun onShareClicked(view: ImageView, movieTitle: String) {
        viewModel.startSharing(view, movieTitle)
    }

    override fun showToastMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
