package com.crycl.abdel_samie.home

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.crycl.abdel_samie.R
import com.crycl.abdel_samie.favourite_movies.FavouriteMoviesActivity
import com.crycl.abdel_samie.search.SearchActivity

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    fun onClick(view: View) {
        when (view.id) {
            R.id.movies -> startActivity(Intent(this, SearchActivity::class.java))
            R.id.favourites -> startActivity(Intent(this, FavouriteMoviesActivity::class.java))
        }
    }
}
