package com.crycl.abdel_samie

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

class BindingUtils {
    companion object {
        /*
            set photo from remote server to imageView
         */
        @JvmStatic
        @BindingAdapter("image_src")
        fun ImageView.setLoadImage(photo: String?) {
            Glide.with(this.context)
                .load(URLs.PHOTOS_BASE + "w185" + photo)
                .error(R.mipmap.ic_launcher)
                .into(this)
        }

        /*
            set favourite imageView if checking if movie is favourite or not
         */
        @JvmStatic
        @BindingAdapter("android:favourite_src")
        fun ImageView.setIcon(isFavourite: Boolean) {
            if (isFavourite)
                this.setImageResource(android.R.drawable.btn_star_big_on)
            else
                this.setImageResource(android.R.drawable.btn_star_big_off)
        }

        /*
            convert double numbers to strings to be able to set them to textView
         */
        @JvmStatic
        @BindingAdapter("android:double_number")
        fun TextView.convertDoubleToString(number: Double) {
            this.text = number.toString()
        }
    }
}