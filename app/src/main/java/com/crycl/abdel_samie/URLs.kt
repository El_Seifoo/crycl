package com.crycl.abdel_samie

class URLs {
    companion object {
        const val PHOTOS_BASE = "http://image.tmdb.org/t/p/"
        const val ROOT = "http://api.themoviedb.org/3/"
        const val SEARCH = "search/movie"
    }
}