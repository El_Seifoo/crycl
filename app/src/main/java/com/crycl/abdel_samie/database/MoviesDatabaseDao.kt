package com.crycl.abdel_samie.database

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.crycl.abdel_samie.models.Movie

@Dao
interface MoviesDatabaseDao {

    // insert movie into the table
    @Insert
    fun insert(movie: Movie)

    // delete movie from the table
    @Delete
    fun removeMovie(movie: Movie)

    // fetch movie by id
    @Query("SELECT * FROM favourite_movies_table WHERE id=:id")
    fun getMovieById(id: String): Movie

    // retrieve all movies in the table
    @Query("SELECT * FROM favourite_movies_table")
    fun getAllMovies(): MutableList<Movie>
}