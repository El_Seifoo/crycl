package com.crycl.abdel_samie.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.crycl.abdel_samie.models.Movie

@Database(entities = [Movie::class], version = 5, exportSchema = false)
abstract class MoviesDatabase : RoomDatabase() {
    // connects db to the dao
    abstract val moviesDatabaseDao: MoviesDatabaseDao

    companion object {
        @Volatile
        private var INSTANCE: MoviesDatabase? = null

        fun getInstance(context: Context): MoviesDatabase {
            synchronized(this) {
                var instance =
                    INSTANCE

                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        MoviesDatabase::class.java,
                        "movies_database"
                    )
                        .fallbackToDestructiveMigration()
                        .build()

                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}