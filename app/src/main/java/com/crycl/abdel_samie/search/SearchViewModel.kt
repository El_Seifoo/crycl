package com.crycl.abdel_samie.search

import android.app.Activity
import android.app.Application
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.core.content.FileProvider
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.crycl.abdel_samie.MySingleton
import com.crycl.abdel_samie.R
import com.crycl.abdel_samie.database.MoviesDatabaseDao
import com.crycl.abdel_samie.models.ApiListResponse
import com.crycl.abdel_samie.models.Movie
import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class SearchViewModel(
    val database: MoviesDatabaseDao,
    application: Application
) : AndroidViewModel(application) {

    // communicator between viewModel and its view
    lateinit var viewListener: ViewListener

    // viewModelJob allows us to cancel all coroutines started by this ViewModel.
    private val viewModelJob = Job()

    /*
        Coroutine scope keeps track of all coroutines started by this ViewModel.

        because of passing viewModelJob any coroutine started in this uiScope can be cancelled by calling `viewModelJob.cancel()`

        all coroutines started in uiScope will launch in [Dispatchers.Main] which is the main thread on Android
     */
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)


    // flag to be able to handle progress bar only if req. the first page (if app is loading data or not)
    private val _progress = MutableLiveData<Int>(View.GONE)
    val progress: LiveData<Int>
        get() = _progress

    // flag to know if there is any error or not (if yes error view is visible)
    private val _errorView = MutableLiveData<Int>(View.GONE)
    val errorView: LiveData<Int>
        get() = _errorView

    // carry error message from server to display to user by errorView
    private val _errorMessage = MutableLiveData<String>("")
    val errorMessage: LiveData<String>
        get() = _errorMessage

    // flag to know if retrieved movies list from server is empty or not
    private val _emptyListTextView = MutableLiveData<Int>(View.VISIBLE)
    val emptyListTextView: LiveData<Int>
        get() = _emptyListTextView

    // flag to be able to know if user is requesting movies or not
    var isLoading = false
    var page = 0
    var query = ""

    private val _moviesList = MutableLiveData<MutableList<Movie>>()
    val moviesList: LiveData<MutableList<Movie>>
        get() = _moviesList

    // req. movies list using query to search
    fun requestMovies(query: String): LiveData<MutableList<Movie>> {
        // if the current query is not equal the prev. query that means user is searching for new word
        // so we clear the list and reset page number
        if (!query.equals(this.query)) {
            this.page = 0
            viewListener.resetAdapter()
            _moviesList.value = null
            _emptyListTextView.value = View.VISIBLE
        }


        if (_moviesList.value == null) {
            this.query = query
            isLoading = true
            this.page++
            loadMovies()
        } else if (_moviesList.value != null && page > 0) {
            this.query = query
            isLoading = true
            this.page++
            loadMovies()
        }
        return moviesList
    }

    // fetch movies by query
    private fun loadMovies() {
        if (page == 1) _progress.value = View.VISIBLE

        val call = MySingleton.getInstance().createService().search(
            page,
            query,
            false,
            getApplication<Application>().getString(R.string.api_key)
        )

        call.enqueue(object : Callback<ApiListResponse<Movie>> {

            override fun onResponse(
                call: Call<ApiListResponse<Movie>>,
                response: Response<ApiListResponse<Movie>>
            ) {

                if (page == 1) _progress.value = View.GONE
                isLoading = false
                val responseCode = response.code()
                if (responseCode == 401 || responseCode == 404) {
                    handleRequestResponse(responseCode, page)
                    return
                }

                if (responseCode == 200) {
                    val moviesList = response.body()?.list
                    when {
                        moviesList == null -> {
                            if (page == 1) {
                                _moviesList.value = mutableListOf()
                                _emptyListTextView.value = View.VISIBLE
                            }

                        }
                        moviesList.isEmpty() -> {
                            if (page == 1)
                                _emptyListTextView.value = View.VISIBLE
                        }
                        else -> {
                            _emptyListTextView.value = View.GONE
                            if (page == 1)
                                _moviesList.value = moviesList
                            else {
                                var list = _moviesList.value
                                list?.addAll(moviesList)
                                _moviesList.postValue(list)
                            }

                        }
                    }
                }

            }

            // handle req. error message depending on the failure message of the req.
            override fun onFailure(call: Call<ApiListResponse<Movie>>, t: Throwable) {
                isLoading = false
                if (page == 1) {
                    _moviesList.value = null
                    _progress.value = View.GONE
                    viewListener.showToastMessage(returnErrorMessage(t))
                } else {
                    _errorView.value = View.VISIBLE
                    _errorMessage.value = returnErrorMessage(t)
                }
            }
        })
    }

    // handle req. error messages depending on request code
    private fun handleRequestResponse(respCode: Int, page: Int) {
        if (page > 1) {
            _moviesList.value = null
            viewListener.showToastMessage(returnErrorMessage(respCode))
        } else {
            _errorView.value = View.VISIBLE
            _errorMessage.value = returnErrorMessage(respCode)
        }
    }

    private fun returnErrorMessage(responseCode: Int): String {
        if (responseCode == 401) return getApplication<Application>().getString(R.string.invalid_api_key)
        return getApplication<Application>().getString(R.string.api_not_found)
    }


    private fun returnErrorMessage(t: Throwable): String {
        if (t is IOException) return (getApplication<Application>().getString(R.string.no_internet_connection))
        return getApplication<Application>().getString(R.string.error_fetching_data)
    }

    // check when recyclerView reach its last item so user can req. the next page
    fun onScrolled(recyclerView: RecyclerView, dy: Int) {
        if (dy > 0) {
            val pastVisibleItem =
                (recyclerView.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition()
            val total = recyclerView.adapter!!.itemCount
            if (!isLoading) {
                if (pastVisibleItem >= total - 1) {
                    this.requestMovies(query)
                }

            }
        }
    }

    fun addRemoveFavourite(movie: Movie, index: Int) {
        movie.favourite = true
        // launch coroutine
        uiScope.launch {
            addRemove(movie, index)
        }
    }


    private suspend fun addRemove(movie: Movie, index: Int) {

        withContext(Dispatchers.IO) {
            // first check if this movie exists in favourits table (by its id)
            if (database.getMovieById(movie.id) == null) {// not exist so insert it
                movie.favourite = true
                database.insert(movie)

            } else {
                // exist so remove it
                movie.favourite = false
                database.removeMovie(movie)
            }
            // update the list to make favourite star on t
            var list = _moviesList.value
            list?.set(index, movie)
            _moviesList.postValue(list)
        }
    }

    override fun onCleared() {
        super.onCleared()
        // cancel all coroutins when activity is destroyed
        viewModelJob.cancel()
    }

    fun startSharing(view: ImageView, movieTitle: String) {
        // Get access to the URI for the bitmap
        val bmpUri = getLocalBitmapUri(view, movieTitle)
        if (bmpUri != null) {
            // Construct a ShareIntent with link to image
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri)
            shareIntent.type = "image/*"
            // Launch sharing dialog for image
            viewListener.startSharing(shareIntent)

        } else {
            // user did'n get bitmap uri, sharing failed
            viewListener.showToastMessage(getApplication<Application>().getString(R.string.something_went_wrong))
        }
    }

    private fun getLocalBitmapUri(imageView: ImageView, movieTitle: String): Uri? {
        // Extract Bitmap from ImageView drawable
        val drawable = imageView.drawable
        var bmp: Bitmap? = null
        bmp = if (drawable is BitmapDrawable) {
            (imageView.drawable as BitmapDrawable).bitmap
        } else {
            return null
        }
        // Store image to default external storage directory
        var bmpUri: Uri? = null
        try {
            // This way, you don't need to request external read/write permission.
            val file = File(
                viewListener.getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                "share_image_$movieTitle.png"
            )
            val out = FileOutputStream(file)
            bmp.compress(Bitmap.CompressFormat.PNG, 100, out)
            out.close()
            bmpUri = FileProvider.getUriForFile(
                viewListener.getActivity().baseContext,
                viewListener.getActivity().applicationContext.packageName + ".provider",
                file
            );
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return bmpUri
    }


    interface ViewListener {
        fun showToastMessage(message: String)
        fun resetAdapter()
        fun startSharing(shareIntent: Intent)
        fun getActivity(): Activity
    }
}