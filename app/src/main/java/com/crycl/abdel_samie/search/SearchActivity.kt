package com.crycl.abdel_samie.search

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.crycl.abdel_samie.MoviesAdapter
import com.crycl.abdel_samie.R
import com.crycl.abdel_samie.database.MoviesDatabase
import com.crycl.abdel_samie.databinding.ActivitySearchBinding
import com.crycl.abdel_samie.models.Movie


class SearchActivity : AppCompatActivity(), SearchViewModel.ViewListener,
    MoviesAdapter.OnItemClicked {
    private lateinit var viewModel: SearchViewModel
    private lateinit var binding: ActivitySearchBinding
    private lateinit var moviesList: RecyclerView
    private lateinit var adapter: MoviesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // initialize data binding
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search)

        // initialize SearchViewModel
        val viewModelFactory = SearchViewModelFactory(
            MoviesDatabase.getInstance(application).moviesDatabaseDao, application
        )
        viewModel = ViewModelProvider(this, viewModelFactory).get(SearchViewModel::class.java)
        viewModel.viewListener = this
        // setting dataBinding variables
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        // initialize recycler view
        moviesList = binding.moviesList
        moviesList.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        adapter = MoviesAdapter(this)
        moviesList.adapter = adapter
        // listen to scroll action
        moviesList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                viewModel.onScrolled(recyclerView, dy)
            }
        })
        // call viewModel fn. to make network req. to fetch movies
        viewModel.requestMovies("a").observe(this,
            Observer<List<Movie>> { movies -> adapter.setMovies(movies) })

        val actionBar = supportActionBar
        actionBar!!.setDisplayHomeAsUpEnabled(true)
        actionBar!!.title = ""
    }

    override fun showToastMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun resetAdapter() {
        adapter.clear()
    }


    override fun startSharing(shareIntent: Intent) {
        startActivity(Intent.createChooser(shareIntent, getString(R.string.share_poster_via)))
    }

    override fun getActivity() = this

    override fun onClick(movie: Movie, index: Int) {
        viewModel.addRemoveFavourite(movie, index);
    }


    override fun onShareClicked(view: ImageView, movieTitle: String) {
        viewModel.startSharing(view, movieTitle)
    }


    // initialize search view in optionsMenu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_activity_action_menu, menu)

        var menuItem = menu!!.findItem(R.id.action_search)
        var searchView: SearchView = menuItem.actionView as SearchView

        // add listener to searchView to make req. when query change
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query != null)
                    viewModel.requestMovies(query)

                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText != null)
                    viewModel.requestMovies(newText)

                return true
            }

        })

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }


}
