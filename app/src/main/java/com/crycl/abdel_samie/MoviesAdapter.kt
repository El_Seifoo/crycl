package com.crycl.abdel_samie

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.crycl.abdel_samie.databinding.MoviesListItemBinding
import com.crycl.abdel_samie.models.Movie

class MoviesAdapter(val clickListener: OnItemClicked) :
    RecyclerView.Adapter<MoviesAdapter.Holder>() {
    var list: MutableList<Movie> = mutableListOf()

    fun setMovies(list: List<Movie>?) {
        if (list != null) {
            this.list = list as MutableList<Movie>
            notifyDataSetChanged()
        }
    }


    interface OnItemClicked {
        fun onClick(movie: Movie, index: Int)
        fun onShareClicked(view: ImageView, movieTitle: String)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder.from(parent)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(clickListener, list.get(position), position)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun clear() {
        list.clear()
        notifyDataSetChanged()
    }

    class Holder private constructor(val dataBinding: MoviesListItemBinding) :
        RecyclerView.ViewHolder(dataBinding.root) {

        fun bind(clickListener: OnItemClicked, movie: Movie, position: Int) {
            dataBinding.movie = movie
            dataBinding.index = position
            dataBinding.onClick = clickListener
            dataBinding.onShareClicked = clickListener
            dataBinding.view = dataBinding.photo

            dataBinding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): Holder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = MoviesListItemBinding.inflate(layoutInflater, parent, false)
                return Holder(binding)
            }
        }
    }
}