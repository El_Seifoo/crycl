package com.crycl.abdel_samie

import com.crycl.abdel_samie.models.ApiListResponse
import com.crycl.abdel_samie.models.Movie
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiServicesClient {
    @GET(URLs.SEARCH)
    fun search(
        @Query("page") page: Int,
        @Query("query") query: String,
        @Query("include_adult") includeAdult: Boolean,
        @Query("api_key") apiKey: String
    ): Call<ApiListResponse<Movie>>;
}