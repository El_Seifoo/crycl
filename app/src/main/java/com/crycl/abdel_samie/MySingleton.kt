package com.crycl.abdel_samie

import android.app.Application
import androidx.databinding.library.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MySingleton : Application {

    companion object {
        var mInstance: MySingleton? = null
        fun getInstance(): MySingleton {
            if (mInstance == null)
                mInstance = MySingleton()

            return mInstance as MySingleton
        }

    }


    private var retrofit: Retrofit

    private constructor() {
        this.retrofit = getRetrofit()
    }

    private fun getRetrofit(): Retrofit {
        if (retrofit == null) {
            var okHttpClient = OkHttpClient.Builder()
            var loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = (HttpLoggingInterceptor.Level.BODY)

//            if (BuildConfig.DEBUG)
                okHttpClient.addInterceptor(loggingInterceptor)

            var builder = Retrofit.Builder()
                .baseUrl(URLs.ROOT)
                .client(okHttpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
            retrofit = builder.build()
        }

        return retrofit
    }

    public fun createService(): ApiServicesClient {
        return retrofit.create(ApiServicesClient::class.java)
    }


}