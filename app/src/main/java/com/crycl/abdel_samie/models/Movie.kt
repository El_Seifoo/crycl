package com.crycl.abdel_samie.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "favourite_movies_table")
class Movie(
    @PrimaryKey() var id: String,
    @ColumnInfo(name = "movie_title") val title: String,
    @ColumnInfo(name = "movie_poster") @SerializedName("poster_path") val poster: String?,
    @ColumnInfo(name = "overview") val overview: String,
    @ColumnInfo(name = "movie_rate") @SerializedName("vote_average") val rate: Double,
    @ColumnInfo(name = "release_date") @SerializedName("release_date") val releaseDate: String,
    @ColumnInfo(name = "favourite") var favourite: Boolean
)