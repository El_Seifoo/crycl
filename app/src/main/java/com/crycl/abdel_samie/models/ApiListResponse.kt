package com.crycl.abdel_samie.models

import com.google.gson.annotations.SerializedName

class ApiListResponse<T> {
    @SerializedName("total_pages")
    val totalPages: Int = 0

    @SerializedName("results")
    lateinit var list: MutableList<T>
}